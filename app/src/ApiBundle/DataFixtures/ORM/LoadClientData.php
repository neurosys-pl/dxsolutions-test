<?php

namespace UserBundle\DataFixtures\ORM;

use AppBundle\Entity\AccessToken;
use AppBundle\Entity\Client;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadClientData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $client = new Client();
        $client->setName('Website');
        $client->setRedirectUris(['http://dx-solutions.dev']);
        $client->setAllowedGrantTypes(['password']);
        $manager->persist($client);

        $this->addReference('client-website', $client);

        $token = new AccessToken($client, $this->getReference('user-test'));
        $token->setToken('user-test');
        $token->setClient($client);
        $manager->persist($token);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }
}
