<?php
namespace ApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateClientCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('api:create-client')
            ->setDescription('Creates oauth client.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $clientManager = $this->getContainer()->get('fos_oauth_server.client_manager.default');
        $client = $clientManager->createClient();
        $client->setRedirectUris(['http://www.dxsolution.com']);
        $client->setAllowedGrantTypes(['access_token', 'token', 'password', 'refresh_token']);
        $clientManager->updateClient($client);

        $output->writeln($client->getPublicId());
        $output->writeln($client->getSecret());

    }
}