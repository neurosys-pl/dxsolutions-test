<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Job;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class JobsController extends FOSRestController
{
    public function getJobsAction()
    {
        $data = $this->getDoctrine()->getEntityManager()->getRepository('AppBundle:Job')->findAll();
        $view = $this->view($data, 200);

        return $this->handleView($view);
    }

    public function getJobAction(Request $request)
    {
        $data = $this->getDoctrine()
            ->getEntityManager()
            ->getRepository('AppBundle:Job')
            ->find($request->get('id'));

        if (!$data) {
            throw new HttpException(404, "Job not found.");
        }

        $view = $this->view($data, 200);

        return $this->handleView($view);
    }

    public function postCreateAction(Request $request)
    {
        $job = new Job();
        $form = $this->createForm('AppBundle\Form\JobType', $job, ['csrf_protection' => false]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($job);
            $em->flush($job);

            $view = $this->view(['success' => true], 200);

            return $this->handleView($view);
        }

        throw new HttpException(404, $form->getErrors());
    }

    public function postUpdateAction(Request $request, Job $job)
    {
        $form = $this->createForm('AppBundle\Form\JobType', $job, ['csrf_protection' => false]);
        $form->submit($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush($job);

            $view = $this->view(['success' => true], 200);

            return $this->handleView($view);
        }

        throw new HttpException(404, $form->getErrors());
    }

    public function deleteDeleteAction(Request $request, Job $job)
    {
        $form = $this->createDeleteForm($job);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($job);
            $em->flush($job);

            $view = $this->view(['success' => true], 200);

            return $this->handleView($view);
        }

        throw new HttpException(404, $form->getErrors());
    }

    private function createDeleteForm(Job $job)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('job_delete', array('id' => $job->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

}
