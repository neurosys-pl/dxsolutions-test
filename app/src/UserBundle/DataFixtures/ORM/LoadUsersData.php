<?php

namespace UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use UserBundle\Entity\User;

class LoadUsersData implements FixtureInterface {

    public function load(ObjectManager $manager) {

        $user = new User();
        $user->setUsername('lukasz');
        $user->setEmail('lukasz@lukasz.pl');
        $user->setEnabled('1');
        $user->setPlainPassword('lukasz');
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);

        $this->addReference('user-test', $user);

        $manager->flush();
    }

}
